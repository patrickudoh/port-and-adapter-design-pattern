package com.accountservice.account.entity

import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name="account")
class AccountJPA(@Id val id:String, val name:String, val balance: BigDecimal) {

}