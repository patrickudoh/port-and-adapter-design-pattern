package com.accountservice.account.entity

import com.accountservice.account.repository.AccountRepository
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.util.*

@Component
class CreateAccount(val accountRepository: AccountRepository){
    fun execute(name: String): Account {
        val newAccount = Account(UUID.randomUUID().toString(), name, BigDecimal.valueOf(0.0))
        accountRepository.save(newAccount)

        return newAccount
    }
}