package com.accountservice.account.entity

import com.accountservice.account.repository.AccountRepository
import org.springframework.stereotype.Component

@Component
class GetAllAccount(val accountRepository: AccountRepository)  {
    fun execute(): List<Account> {
        return accountRepository.findAll()
    }
}