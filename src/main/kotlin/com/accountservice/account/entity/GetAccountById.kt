package com.accountservice.account.entity

import com.accountservice.account.repository.AccountRepository
import org.springframework.stereotype.Component

@Component
class GetAccountById(val accountRepository: AccountRepository) {
    fun execute(id: String): Account? {
        return accountRepository.findById(id)
    }
}