package com.accountservice.account.entity

import com.accountservice.account.repository.AccountRepository
import org.springframework.stereotype.Component

@Component
class DeleteAccount(val accountRepository: AccountRepository) {
    fun execute(id: String) {
        accountRepository.delete(id)
    }
}