package com.accountservice.account.entity

import com.accountservice.account.repository.AccountRepository

import org.springframework.stereotype.Component


@Component
class UpdateAccount(val accountRepository: AccountRepository) {
    fun execute(id: String, newName: String): Account {
        val account = accountRepository.findById(id)

        if (account != null) {
            val updatedAccount = Account(id, newName, account.balance)
            accountRepository.save(updatedAccount)
            return updatedAccount
        } else {
            throw Exception("No role with id: $id")
        }
    }
}