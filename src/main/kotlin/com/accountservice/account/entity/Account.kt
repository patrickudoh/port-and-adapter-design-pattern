package com.accountservice.account.entity

import java.math.BigDecimal

data class Account(val id:String, val name:String, val balance: BigDecimal) {}