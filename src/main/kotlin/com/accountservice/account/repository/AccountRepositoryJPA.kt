package com.accountservice.account.repository

import com.accountservice.account.entity.AccountJPA
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AccountRepositoryJPA: JpaRepository<AccountJPA, String> {
}