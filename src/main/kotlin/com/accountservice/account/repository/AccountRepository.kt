package com.accountservice.account.repository

import com.accountservice.account.entity.Account
import org.springframework.stereotype.Repository

@Repository
interface AccountRepository {
    fun findById(id: String): Account?
    fun save(account: Account)
    fun findAll(): List<Account>
    fun delete(id: String)
}