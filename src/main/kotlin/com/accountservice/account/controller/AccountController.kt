package com.accountservice.account.controller

import com.accountservice.account.entity.*
import com.accountservice.account.resources.AccountResource
import com.accountservice.account.resources.AccountResourceWithId
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
class AccountController (
    private val getAllAccount: GetAllAccount,
    private val getAccountById: GetAccountById,
    private val updateAccount: UpdateAccount,
    private val deleteAccount: DeleteAccount,
    private val createAccount: CreateAccount
){

    @PostMapping("/accounts")
    @ResponseStatus(HttpStatus.CREATED)
    fun createAccount(@RequestBody accountResource: AccountResource): AccountResourceWithId {

        val savedAccount = createAccount.execute(accountResource.name)

        return AccountResourceWithId(savedAccount.id, savedAccount.name, savedAccount.balance)
    }

    @GetMapping("/accounts/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun getAccountById(@PathVariable("id") id:String): AccountResourceWithId {
        val retrievedAccount = getAccountById.execute(id) ?: throw Exception("No role with id: $id")

        return AccountResourceWithId(retrievedAccount!!.id, retrievedAccount.name, retrievedAccount.balance)

    }

    @GetMapping("/accounts")
    @ResponseStatus(HttpStatus.OK)
    fun getAllAccounts(): List<AccountResourceWithId>{
        return getAllAccount.execute().map { account ->

            AccountResourceWithId(account.id, account.name, account.balance)

        }
    }

    @PutMapping("/accounts/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun updateAccount(@RequestBody accountResource: AccountResource, @PathVariable("id") id:String): AccountResourceWithId {

        val updatedAccount = updateAccount.execute(id, accountResource.name)

        return AccountResourceWithId(updatedAccount.id, updatedAccount.name, updatedAccount.balance)

    }

    @DeleteMapping("/accounts/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteAccount(@PathVariable("id") id:String){
        deleteAccount.execute(id)
    }

}