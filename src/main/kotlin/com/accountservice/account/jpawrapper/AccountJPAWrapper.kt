package com.accountservice.account.jpawrapper

import com.accountservice.account.entity.Account
import com.accountservice.account.entity.AccountJPA
import com.accountservice.account.repository.AccountRepository
import com.accountservice.account.repository.AccountRepositoryJPA


import org.springframework.data.repository.findByIdOrNull

class AccountJPAWrapper(private val accountRepositoryJPA: AccountRepositoryJPA): AccountRepository {
    override fun findById(id: String): Account? {

        val accountJPA = accountRepositoryJPA.findByIdOrNull(id)
        val account = if (accountJPA != null) {

          Account(accountJPA.id, accountJPA.name, accountJPA.balance)
        } else null

        return account
    }

    override fun save(account: Account) {
        val accountJPA = AccountJPA(account.id, account.name, account.balance)
        accountRepositoryJPA.save(accountJPA)
    }

    override fun findAll(): List<Account> {
        val accountJPA =  accountRepositoryJPA.findAll()
        return accountJPA.map { Account(it.id, it.name, it.balance) }
    }

    override fun delete(id: String) {
       return accountRepositoryJPA.deleteById(id)
    }

}