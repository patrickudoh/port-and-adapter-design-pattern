package com.accountservice.account

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@ComponentScan("com.accountservice.account.controller", "com.accountservice.account.entity.GetAllAccount", "com.accountservice.account.repository.AccountRepository")
@EnableJpaRepositories("com.accountservice.account.repository.AccountRepository")
@EntityScan("com.accountservice.account.entity")
class AccountApplication
fun main(args: Array<String>) {
	runApplication<AccountApplication>(*args)
}
