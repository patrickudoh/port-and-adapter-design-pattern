package com.accountservice.account.resources

import java.math.BigDecimal

class AccountResource(val id:String, val name:String, val balance:BigDecimal =  BigDecimal.valueOf(0.0)){

}